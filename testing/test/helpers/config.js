import path from 'path';

const sauceCaps = {};
// const iosCaps = {
//   platformName: 'iOS',
//   automationName: 'XCUITest',
//   deviceName: process.env.IOS_DEVICE_NAME || 'iPhone 6',
//   platformVersion: process.env.IOS_PLATFORM_VERSION || '12.0',
//   app: undefined, // Will be added in tests
//   // appiumVersion: '1.12.1',
//   showIOSLog: false,
//   ... sauceCaps,
// };

// Leave the Android platformVersion blank and set deviceName to a random string
// (Android deviceName is ignored by Appium but is still required)
// If we're using SauceLabs, set the Android deviceName and platformVersion to
// the latest supported SauceLabs device and version
const DEFAULT_ANDROID_DEVICE_NAME = process.env.SAUCE_LABS ? 'Android GoogleAPI Emulator' : 'My Android Device';
const DEFAULT_ANDROID_PLATFORM_VERSION = process.env.SAUCE_LABS ? '7.1' : null;

const androidCaps = {
  platformName: 'Android',
  automationName: 'UiAutomator2',
  deviceName: process.env.ANDROID_DEVICE_NAME || DEFAULT_ANDROID_DEVICE_NAME,
  platformVersion: process.env.ANDROID_PLATFORM_VERSION || DEFAULT_ANDROID_PLATFORM_VERSION,
  app: undefined, // Will be added in tests
  ...sauceCaps,
};

// figure out where the Appium server should be pointing to
const serverConfig = {
  host: process.env.APPIUM_HOST || 'localhost',
  port: process.env.APPIUM_PORT || 4723
};


// figure out the location of the apps under test
const LOCAL_ASSET_BASE = path.resolve(__dirname, '..', '..', 'apps');

// const iosTestApp = path.resolve(LOCAL_ASSET_BASE, 'TestApp.app.zip');
const androidApiDemos = path.resolve(LOCAL_ASSET_BASE, 'instafood.apk');

export {
  androidApiDemos,
  androidCaps, sauceCaps,
  serverConfig
};
